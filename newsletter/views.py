import datetime
import logging

logger = logging.getLogger(__name__)

from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.conf import settings
from django.core.xheaders import populate_xheaders
from django.template.response import TemplateResponse

from django.template import loader, RequestContext, Context

from django.shortcuts import get_object_or_404
from django.http import Http404

from django.contrib import messages
from django.contrib.sites.models import Site
from django.contrib.auth.decorators import login_required

from django.utils.translation import ugettext, ugettext_lazy as _

from django.forms.models import modelformset_factory

from .models import Newsletter, Subscription, Submission, EmailTemplate
from .forms import (
    SubscribeRequestForm, UserUpdateForm, UpdateRequestForm,
    UnsubscribeRequestForm, UpdateForm
)


def render(request, templates, dictionary=None, context_instance=None,
           **kwargs):
    """
    Mimics ``django.shortcuts.render`` but uses a TemplateResponse for
    ``mezzanine.core.middleware.TemplateForDeviceMiddleware``
    """
    dictionary = dictionary or {}
    if context_instance:
        context_instance.update(dictionary)
    else:
        context_instance = RequestContext(request, dictionary)
    return TemplateResponse(request, templates, context_instance, **kwargs)


def archive_index(request, queryset, date_field, num_latest=15,
                  template_name=None, template_loader=loader,
                  extra_context=None, allow_empty=True,
                  context_processors=None, mimetype=None,
                  allow_future=False, template_object_name='latest'):
    """
    Generic top-level archive of date-based objects.

    Templates: ``<app_label>/<model_name>_archive.html``
    Context:
        date_list
            List of years
        latest
            Latest N (defaults to 15) objects by date
    """
    if extra_context is None:
        extra_context = {}
    model = queryset.model
    if not allow_future:
        queryset = queryset.filter(**{'%s__lte' % date_field: datetime.datetime.now()})
    date_list = queryset.dates(date_field, 'year')[::-1]
    if not date_list and not allow_empty:
        raise Http404("No %s available" % model._meta.verbose_name)

    if date_list and num_latest:
        latest = queryset.order_by('-' + date_field)[:num_latest]
    else:
        latest = None

    if not template_name:
        template_name = "%s/%s_archive.html" % (model._meta.app_label, model._meta.object_name.lower())
    c = RequestContext(request, {
        'date_list': date_list,
        template_object_name: latest,
    }, context_processors)
    for key, value in extra_context.items():
        if callable(value):
            c[key] = value()
        else:
            c[key] = value
    return TemplateResponse(request, template_name, c, mimetype=mimetype)


def object_detail(request, queryset, object_id=None, slug=None,
                  slug_field='slug', template_name=None,
                  template_name_field=None, template_loader=loader,
                  extra_context=None, context_processors=None,
                  template_object_name='object', mimetype=None):
    """
    Generic detail of an object.

    Templates: ``<app_label>/<model_name>_detail.html``
    Context:
        object
            the object
    """
    if extra_context is None:
        extra_context = {}
    model = queryset.model
    if object_id:
        queryset = queryset.filter(pk=object_id)
    elif slug and slug_field:
        queryset = queryset.filter(**{slug_field: slug})
    else:
        raise AttributeError("Generic detail view must be called with either an object_id or a slug/slug_field.")
    try:
        obj = queryset.get()
    except ObjectDoesNotExist:
        raise Http404("No %s found matching the query" % (model._meta.verbose_name))
    if not template_name:
        template_name = "%s/%s_detail.html" % (model._meta.app_label, model._meta.object_name.lower())
    if template_name_field:
        template_name_list = [getattr(obj, template_name_field), template_name]
    else:
        template_name_list=[template_name]
    c = RequestContext(request, {
        template_object_name: obj,
    }, context_processors)
    for key, value in extra_context.items():
        if callable(value):
            c[key] = value()
        else:
            c[key] = value
    response = TemplateResponse(request, template_name_list, c,
                                mimetype=mimetype)
    populate_xheaders(request, response, model,
                      getattr(obj, obj._meta.pk.name))
    return response


def object_list(request, queryset, paginate_by=None, page=None,
                allow_empty=True, template_name=None, template_loader=loader,
                extra_context=None, context_processors=None,
                template_object_name='object', mimetype=None):
    """
    Generic list of objects.

    Templates: ``<app_label>/<model_name>_list.html``
    Context:
        object_list
            list of objects
        is_paginated
            are the results paginated?
        results_per_page
            number of objects per page (if paginated)
        has_next
            is there a next page?
        has_previous
            is there a prev page?
        page
            the current page
        next
            the next page
        previous
            the previous page
        pages
            number of pages, total
        hits
            number of objects, total
        last_on_page
            the result number of the last of object in the
            object_list (1-indexed)
        first_on_page
            the result number of the first object in the
            object_list (1-indexed)
        page_range:
            A list of the page numbers (1-indexed).
    """
    if extra_context is None:
        extra_context = {}
    queryset = queryset._clone()
    if paginate_by:
        paginator = Paginator(queryset,
                              paginate_by,
                              allow_empty_first_page=allow_empty)
        if not page:
            page = request.GET.get('page', 1)
        try:
            page_number = int(page)
        except ValueError:
            if page == 'last':
                page_number = paginator.num_pages
            else:
                # Page is not 'last', nor can it be converted to an int.
                raise Http404
        try:
            page_obj = paginator.page(page_number)
        except InvalidPage:
            raise Http404
        c = RequestContext(request, {
            '%s_list' % template_object_name: page_obj.object_list,
            'paginator': paginator,
            'page_obj': page_obj,
            'is_paginated': page_obj.has_other_pages(),

            # Legacy template context stuff. New templates should use page_obj
            # to access this instead.
            'results_per_page': paginator.per_page,
            'has_next': page_obj.has_next(),
            'has_previous': page_obj.has_previous(),
            'page': page_obj.number,
            'next': page_obj.next_page_number(),
            'previous': page_obj.previous_page_number(),
            'first_on_page': page_obj.start_index(),
            'last_on_page': page_obj.end_index(),
            'pages': paginator.num_pages,
            'hits': paginator.count,
            'page_range': paginator.page_range,
        }, context_processors)
    else:
        c = RequestContext(request, {
            '%s_list' % template_object_name: queryset,
            'paginator': None,
            'page_obj': None,
            'is_paginated': False,
        }, context_processors)
        if not allow_empty and len(queryset) == 0:
            raise Http404
    for key, value in extra_context.items():
        if callable(value):
            c[key] = value()
        else:
            c[key] = value
    if not template_name:
        model = queryset.model
        template_name = "%s/%s_list.html" % (model._meta.app_label, model._meta.object_name.lower())
    return TemplateResponse(request, template_name, c, mimetype=mimetype)


def newsletter_list(request):
    newsletters = Newsletter.on_site.filter(visible=True)

    if not newsletters:
        raise Http404

    if request.user.is_authenticated():
        SubscriptionFormSet = modelformset_factory(
            Subscription, form=UserUpdateForm, extra=0)

        for n in newsletters:
            Subscription.objects.get_or_create(
                newsletter=n, user=request.user)

        qs = Subscription.objects.filter(
            newsletter__in=newsletters, user=request.user)

        if request.method == 'POST':
            try:
                formset = SubscriptionFormSet(request.POST, queryset=qs)

                if not formset.is_valid():
                    raise ValidationError('Update form invalid.')

                # Everything's allright, let's save
                formset.save()

                messages.info(request,
                    ugettext("Your changes have been saved."))

            except ValidationError:
                # Invalid form posted. As there is no way for a user to
                # enter data - invalid forms should be ignored from the UI.

                # However, we log them for debugging purposes.
                logger.warning('Invalid form post received',
                    exc_info=True, extra={'request': request})

                # Present a pristine form
                formset = SubscriptionFormSet(queryset=qs)

        else:
            formset = SubscriptionFormSet(queryset=qs)

    else:
        formset = None

    return object_list(
        request, newsletters, extra_context={'formset': formset})


def newsletter_detail(request, newsletter_slug):
    newsletters = Newsletter.on_site.filter(visible=True)

    return object_detail(
        request, newsletters, slug=newsletter_slug)


@login_required
def subscribe_user(request, newsletter_slug, confirm=False):
    my_newsletter = get_object_or_404(
        Newsletter.on_site, slug=newsletter_slug
    )

    already_subscribed = False
    instance = Subscription.objects.get_or_create(
        newsletter=my_newsletter, user=request.user
    )[0]

    if instance.subscribed:
        already_subscribed = True
    elif confirm:
        instance.subscribed = True
        instance.save()

        messages.success(
            request, _('You have been subscribed to %s.') % my_newsletter)

        logger.debug(
            _('User %(rs)s subscribed to %(my_newsletter)s.'), {
                "rs": request.user,
                "my_newsletter": my_newsletter
        })

    if already_subscribed:
        messages.info(
            request, _('You are already subscribed to %s.') % my_newsletter)

    env = {
        'newsletter': my_newsletter,
        'action': 'subscribe'
    }

    return render(
        request=request,
        templates="newsletter/subscription_subscribe_user.html",
        dictionary=env, context_instance=RequestContext(request))


@login_required
def unsubscribe_user(request, newsletter_slug, confirm=False):
    my_newsletter = get_object_or_404(
        Newsletter.on_site, slug=newsletter_slug
    )

    not_subscribed = False

    try:
        instance = Subscription.objects.get(
            newsletter=my_newsletter, user=request.user
        )

        if not instance.subscribed:
            not_subscribed = True
        elif confirm:
            instance.subscribed = False
            instance.save()

            messages.success(
                request,
                _('You have been unsubscribed from %s.') % my_newsletter
            )

            logger.debug(
                _('User %(rs)s unsubscribed from %(my_newsletter)s.'), {
                    "rs": request.user,
                    "my_newsletter": my_newsletter
            })

    except Subscription.DoesNotExist:
        # TODO: Test coverage for this branch
        not_subscribed = True

    if not_subscribed:
        messages.info(request,
            _('You are not subscribed to %s.') % my_newsletter)

    env = {
        'newsletter': my_newsletter,
        'action': 'unsubscribe'
    }

    return render(
        request=request,
        templates="newsletter/subscription_unsubscribe_user.html",
        dictionary=env, context_instance=RequestContext(request))


def subscribe_request(request, newsletter_slug, confirm=False):
    if request.user.is_authenticated():
        return subscribe_user(request, newsletter_slug, confirm)

    my_newsletter = get_object_or_404(
        Newsletter.on_site, slug=newsletter_slug)

    error = None
    if request.POST:
        form = SubscribeRequestForm(
            request.POST,
            newsletter=my_newsletter,
            ip=request.META.get('REMOTE_ADDR')
        )

        if form.is_valid():
            instance = form.save()

            try:
                instance.send_activation_email(action='subscribe')

            except Exception, e:
                logger.exception('Error %s while submitting email to %s.',
                    e, instance.email)
                error = True

    else:
        form = SubscribeRequestForm(newsletter=my_newsletter)

    env = {
        'newsletter': my_newsletter,
        'form': form,
        'error': error,
        'action': 'subscribe'
    }

    return render(
        request=request,
        templates="newsletter/subscription_subscribe.html",
        dictionary=env, context_instance=RequestContext(request))


def unsubscribe_request(request, newsletter_slug, confirm=False):
    if request.user.is_authenticated():
        return unsubscribe_user(request, newsletter_slug, confirm)

    my_newsletter = get_object_or_404(
        Newsletter.on_site, slug=newsletter_slug)

    error = None
    if request.POST:
        form = UnsubscribeRequestForm(request.POST, newsletter=my_newsletter)

        if form.is_valid():
            instance = form.instance

            try:
                instance.send_activation_email(action='unsubscribe')

            except Exception, e:
                logger.exception(
                    'Error %s while submitting email to %s.',
                    e, instance.email)
                error = True
    else:
        form = UnsubscribeRequestForm(newsletter=my_newsletter)

    env = {
        'newsletter': my_newsletter,
        'form': form,
        'error': error,
        'action': 'unsubscribe'
    }

    return render(
        request=request,
        templates="newsletter/subscription_unsubscribe.html",
        dictionary=env, context_instance=RequestContext(request))


def update_request(request, newsletter_slug):
    my_newsletter = get_object_or_404(
        Newsletter.on_site, slug=newsletter_slug)

    error = None
    if request.POST:
        form = UpdateRequestForm(request.POST, newsletter=my_newsletter)
        if form.is_valid():
            instance = form.instance
            try:
                instance.send_activation_email(action='update')

            except Exception, e:
                logger.exception(
                    'Error %s while submitting email to %s.',
                    e, instance.email)
                error = True
    else:
        form = UpdateRequestForm(newsletter=my_newsletter)

    env = {
        'newsletter': my_newsletter,
        'form': form,
        'error': error,
        'action': 'update'
    }

    return render(
        request=request,
        templates="newsletter/subscription_update.html",
        dictionary=env, context_instance=RequestContext(request))


def update_subscription(request, newsletter_slug,
        email, action, activation_code=None):

    assert action in ['subscribe', 'update', 'unsubscribe']

    my_newsletter = get_object_or_404(Newsletter.on_site, slug=newsletter_slug)
    my_subscription = get_object_or_404(
        Subscription, newsletter=my_newsletter, email_field__exact=email
    )

    if activation_code:
        my_initial = {'user_activation_code': activation_code}
    else:
        # TODO: Test coverage of this branch
        my_initial = None

    if request.POST:
        form = UpdateForm(
            request.POST, newsletter=my_newsletter, instance=my_subscription,
            initial=my_initial
        )
        if form.is_valid():
            # Get our instance, but do not save yet
            subscription = form.save(commit=False)

            # If a new subscription or update, make sure it is subscribed
            # Else, unsubscribe
            if action == 'subscribe' or action == 'update':
                subscription.subscribed = True
            else:
                subscription.unsubscribed = True

            logger.debug(
                _(u'Updated subscription %(subscription)s through the web.'),
                {'subscription': subscription}
            )
            subscription.save()
    else:
        form = UpdateForm(
            newsletter=my_newsletter, instance=my_subscription,
            initial=my_initial
        )

        # TODO: Figure out what the hell this code is doing here.

        # If we are activating and activation code is valid and not already
        # subscribed, activate straight away

        # if action == 'subscribe' and form.is_valid() and not my_subscription.subscribed:
        #     subscription = form.save(commit=False)
        #     subscription.subscribed = True
        #     subscription.save()
        #
        #     logger.debug(_(u'Activated subscription %(subscription)s through the web.') % {'subscription':subscription})

    env = {
        'newsletter': my_newsletter,
        'form': form,
        'action': action
    }

    return render(
        request=request,
        templates="newsletter/subscription_activate.html",
        dictionary=env, context_instance=RequestContext(request)
    )


def archive(request, newsletter_slug):
    my_newsletter = get_object_or_404(
        Newsletter.on_site, slug=newsletter_slug, visible=True
    )

    submissions = Submission.objects.filter(
        newsletter=my_newsletter, publish=True
    )

    return archive_index(
        request,
        queryset=submissions,
        date_field='publish_date',
        extra_context={'newsletter': my_newsletter}
    )


def archive_detail(request, newsletter_slug, year, month, day, slug):
    """ Detail view for Submissions in the archive. """

    my_newsletter = get_object_or_404(
        Newsletter.on_site, slug=newsletter_slug, visible=True
    )

    submission = get_object_or_404(
        Submission,
        newsletter=my_newsletter,
        publish=True,
        publish_date__year=year,
        publish_date__month=month,
        publish_date__day=day,
        message__slug=slug
    )

    message = submission.message
    (subject_template, text_template, html_template) = \
        EmailTemplate.get_templates('message', message.newsletter)

    if not html_template:
        # TODO: Test coverage of this branch
        raise Http404(ugettext('No HTML template associated with the '
                               'newsletter this message belongs to.'))

    c = Context({
        'message': message,
        'site': Site.objects.get_current(),
        'newsletter': message.newsletter,
        'date': submission.publish_date,
        'STATIC_URL': settings.STATIC_URL,
        'MEDIA_URL': settings.MEDIA_URL
    })

    return TemplateResponse(request, html_template, c)
